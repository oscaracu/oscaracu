const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const del = require('del');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const usemin = require('gulp-usemin');
const rev = require('gulp-rev');
const cleanCss = require('gulp-clean-css');
const flatmap = require('gulp-flatmap');
const htmlmin = require('gulp-htmlmin');

gulp.task('sass', async function() {
  gulp.src('./css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'))
});

gulp.task('sass:watch', async function() {
  gulp.watch('./css/*.scss', gulp.parallel('sass'));
});

gulp.task('browser-sync', function() {
  var files = ['./*.html', './css/*.css', './images/*.{png, jpg, jpeg, gif}', './js/*.js']
  browserSync.init(files, {
    server: {
      baseDir: './'
    }
  });
});

gulp.task('default', gulp.parallel('browser-sync', 'sass:watch'));

gulp.task('clean', function() {
  return del(['dist']);
});

gulp.task('copyfonts', async function() {
  gulp.src('node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
    .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('imagemin', function() {
  return gulp.src('./images/*')
    .pipe(imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    }))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('usemin', function() {
  return gulp.src('./*.html')
    .pipe(flatmap(function(stream, file) {
      return stream
        .pipe(usemin({
          css: [rev()],
          html: [function() {
            return htmlmin({
              collapseWhitespace: true
            })
          }],
          js: [uglify(), rev()],
          inlinejs: [uglify()],
          inlinecss: [cleanCss(), 'concat']
        }));
    }))
    .pipe(gulp.dest('dist/'));
});

gulp.task('build', gulp.series('clean', gulp.parallel('copyfonts', 'imagemin', 'usemin')));
