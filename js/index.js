
  $(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 4000
    });
    $("#myModal").on("show.bs.modal", function(e) {
      console.log("El modal se está mostrando");
      $("#saberMas").removeClass("btn-primary");
      $("#saberMas").addClass("btn-secondary");
      $("#saberMas").prop("disabled", true);
    });
    $("#myModal").on("shown.bs.modal", function(e) {
      console.log("El modal se mostró");
    });
    $("#myModal").on("hide.bs.modal", function(e) {
      console.log("El modal se esconde");
    });
    $("#myModal").on("hidden.bs.modal", function(e) {
      console.log("El modal se escondió");
      $("#saberMas").removeClass("btn-secondary");
      $("#saberMas").addClass("btn-primary");
      $("#saberMas").prop("disabled", false);
    });
  });
